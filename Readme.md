## Project creation commands

```bash
npm init -y
npm install express cors mongoose dotenv
npm install -g nodemon
```

### to run project

```bash
npm run dev
```
